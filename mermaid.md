# Mermaid tutorial

Let's go!

## flowchart

### Example 1:

```mermaid
flowchart
    A --> B
    B --> C
```

### Example 2:

```mermaid
flowchart LR
    S[Start] --> A
    A[Enter your email address] --> B{Existing user?}
    B -->|No| C(Enter name)
    C --> D{Accept conditions?}
    D -->|No| A
    D -->|Yes| E(Send email with magic link)
    B -->|Yes| E
    E --> End;
```

## sequenceDiagram

---

```mermaid
sequenceDiagram
    autonumber
    participant Client
    participant OAuthProvider
    participant Server
    Client ->> OAuthProvider: Request access token
    activate OAuthProvider
    OAuthProvider ->> Client: Send access token
    deactivate OAuthProvider
    Client ->> Server: Request resource
    activate Server
    Server ->> OAuthProvider: Validate token
    activate OAuthProvider
    OAuthProvider ->> Server: Token valid
    deactivate OAuthProvider
    Server ->> Client: Send resource
    deactivate Server
```

## classDiagram

| Type   | Description   |
|--------|---------------|
| `<|--` | Inheritance   |
| `*--`  | Composition   |
| `o--`  | Aggregation   |
| `-->`  | Association   |
| `--`   | Link (Solid)  |
| `..>`  | Dependency    |
| `..|>` | Realization   |
| `..`   | Link (Dashed) |

> Source: https://mermaid.js.org/syntax/classDiagram.html#defining-relationship

---

```mermaid
classDiagram
    class Order {
        +OrderStatus status
    }
    class OrderStatus {
        <<enumeration>>
        FAILED
        PENDING
        PAID
    }
    class PaymentProcessor {
        <<interface>>
        -String apiKey
        #connect(String url, JSON header)
        +processPayment(Order order) OrderStatus
    }
    class Customer {
        +String name
    }
    Customer <|-- PrivateCustomer
    Customer <|-- BusinessCustomer
    PaymentProcessor <|-- StripePaymentProcessor
    PaymentProcessor <|-- PayPalPaymentProcessor
    Order o-- Customer
    Car *-- Engine
```

## erDiagram

```mermaid
erDiagram
    Customer ||--o{ Order : places
    Order ||--|{ LineItem : contains
    Customer {
        String id
        String name
    }
    Order {
        String id
        OrderStatus status
    }
    LineItem {
        String code
        String description
    }
```


| Value (left) | Value (right) | Meaning                       |
| ------------ | ------------- | ----------------------------- |
| `|o`         | `o|`          | Zero or one                   |
| `||`         | `||`          | Exactly one                   |
| `}o`         | `o{`          | Zero or more (no upper limit) |
| `}|`         | `|{`          | One or more (no upper limit)  |

> Source: https://mermaid.js.org/syntax/entityRelationshipDiagram.html#relationship-syntax
